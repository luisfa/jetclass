# JetClass

**Just enought technology for my class (JetClass).** With the coronavirus teaching needs to go to online mode... In most cases using platforms not very friendly with privacy. Actually, video is not needed... just voice (mumble) + presentation

**The problem:**
With coronacrisis millons of students can continue learning the way they did, and are commited to embrace the digital learning. This is not as bad, if it were possible. There are many problems even in the most developed countries.

1.  Two speeds network. Internet does not reach with the same speed all houses, even there are some of then with no internet at all. Not all the families have a computer. The devices can not be used by students and parents at the same time. If the student learn the parents do not work online... We can not do much with that...
2. Educative institutions, from Schools to Universities, can not support the data transfer that means all their students joining a class by videoconference. So, they are externalizing the service by going to public clouds: Zoom, that gives data to Facebook; Google Meet, obviously to Google;... Some are paying Webex services... Some of then can afford to improve its service by using Jitsi or a combination of Jitsi and Mattermost. But, in general, teaching is runnig to public clouds without any consideration to privacy (and law).

**The question:** How can we help to teach with low latency (at least, no as much as a videoconference)?

**The propossed solution:** from my point of view as teacher, it is enough (and sometimes better from a pedagogic point of view) to give the students some illustration (presentation via Impress, p.e.) with the explanation in a voice channel. What I am talking about? LibreOffice OnLine / CODE + Mumble web client. To control access I would try to integrate it into NextCloud, giving also the ability to share other kind of contents with students, chats for tutoring and many more...

The main thing would be to integrate:
- a reduced version of [LibreOffice OnLine](https://github.com/LibreOffice/online/tree/master/)/CODE (only one editor, the others only see the same image that the editor sees, in order to follow the explanation in real time);
- a [Mumble web client](https://github.com/Johni0702/mumble-web).
- Both should be integrated into Nextcloud, but also other kind of integrations would be possible (Moodle and so on, of course).

A minimalistic approach could be:
- Etherpad with a plugin like this http://blog.etherpad.org/category/plugin/
- the same [Mumble web client](https://github.com/Johni0702/mumble-web).


I am not developer (I am university professor, IAAL too), so I would help the best way I can...

Of course, videoconference have its use, but is not general affordable. For a pedagigoc point of viw I think short videos are better that long direct streamings. For that purpouse there are many using PeerTube. I think is a good solution.

---

SPANISH VERSION

**Objetivo:**

La idea es integrar en un servidor web un cliente Mumble y un editor de LibreOffice Online. Inicialmente podría funcionar de otro modo: el profesor sube un pdf y éste se descompone por páginas. El profesor puede ir cambiando el contenido de la página que se visualiza en determinada url a voluntad: y la página tener un sistema de recarga... Con todo, la visión más simple es estudiar si el mostrar un editor en "funcionamiento", sin compartir la edición, es plausible. Esa es la opción más potente en cuanto a resultado.
Una opción minimalista podría ser usar Etherpad, un plugin para actualizarlo subiéndole documentos, y embebiendo en él el mismo cliente web de Mumble.
